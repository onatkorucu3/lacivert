﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BasketballBallListHandler : MonoBehaviour
{
    public List<GameObject> basketballBallList;
    public int numberOfBasketballBalls;

    public GameObject basketballBallPrefab;

    RestartMenuController restartMenuController;

    int basketballBallCount = 5;

    private void Awake()
    {
        basketballBallList = CreateBasketballBalls();
        PlaceBasketballBalls(basketballBallList);
    }

    // Start is called before the first frame update
    void Start()
    {
        List<RestartMenuController> objectsInScene = new List<RestartMenuController>();

        foreach (RestartMenuController go in Resources.FindObjectsOfTypeAll(typeof(RestartMenuController)) as RestartMenuController[])
        {
            if (!UnityEditor.EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
                objectsInScene.Add(go);
        }

        restartMenuController = objectsInScene[0];
        restartMenuController.OnRestartButtonPressed += HandleOnRestartButtonPressed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void HandleOnRestartButtonPressed()
    {

        foreach (GameObject basketballBall in basketballBallList)
        {
            basketballBall.SetActive(true);
        }
        //CreateBasketballBalls();
        PlaceBasketballBalls(basketballBallList);
    }

    List<GameObject> CreateBasketballBalls()
    {
        List<GameObject> basketballBallList = new List<GameObject>();

        Debug.Log("basketballBallList.Count is: " + basketballBallList.Count);

        for (int i = 0; i < basketballBallCount; i++)
        {
            GameObject g = Instantiate(basketballBallPrefab);
            basketballBallList.Add(g);
        }
        return basketballBallList;

    }

    void PlaceBasketballBalls(List<GameObject> basketballBallList)
    {
        Vector3 firstBallPosition = new Vector3(0, -0.4f, -3.5f);


        for (int i = 0; i < basketballBallList.Count; i++)
        {
            Debug.Log("basketballBallList.Count is: " + basketballBallList.Count);
            basketballBallList[i].transform.position = firstBallPosition + new Vector3(0, -0.8f * i, 0);
        }
    }
}
