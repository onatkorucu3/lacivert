﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuController : MonoBehaviour
{

    public static bool gameIsPaused;

    public GameObject startMenuUI;
    public GameObject gameUICanvas;

    public GameObject gameController;
    BallDragAndThrow ballDragAndThrow;


    // Start is called before the first frame update
    void Start()
    {
        //gameUICanvas = GameObject.FindGameObjectsWithTag("GameUI")[0];
        //GameObject gameController = GameObject.FindWithTag("GameController");

        ballDragAndThrow = gameController.GetComponent<BallDragAndThrow>();

        FreezeGame();
    }

    void FreezeGame()
    {
        gameObject.SetActive(true);
        Time.timeScale = 0f;
        gameUICanvas.SetActive(false);
        ballDragAndThrow.enabled =false;
        gameIsPaused = true;
    }

    public void StartGame()
    {
        Time.timeScale = 1f;
        gameIsPaused = false;
        gameUICanvas.SetActive(true);
        ballDragAndThrow.enabled = true;
        gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
