﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessfulThrowCountDisplayer : MonoBehaviour
{

    GoalDetector goalDetector;
    public int successfulThrowCount;

    Sprite GREEN_CIRCLE;
    Sprite GRAY_CIRCLE;

    GameObject firstCircle;
    GameObject secondCircle;
    GameObject thirdCircle;

    public Image firstCircleImage;
    public Image secondCircleImage;
    public Image thirdCircleImage;
    RestartMenuController restartMenuController;


    // Start is called before the first frame update
    void Start()
    {

        GREEN_CIRCLE = Resources.Load<Sprite>("greenCircle");
        GRAY_CIRCLE = Resources.Load<Sprite>("grayCircle");

        firstCircle = this.transform.GetChild(0).gameObject;
        firstCircleImage = firstCircle.GetComponent<Image>();

        secondCircle = this.transform.GetChild(1).gameObject;
        secondCircleImage = secondCircle.GetComponent<Image>();

        thirdCircle = this.transform.GetChild(2).gameObject;
        thirdCircleImage = thirdCircle.GetComponent<Image>();

        successfulThrowCount = 0;
        goalDetector = FindObjectOfType<GoalDetector>();
        goalDetector.OnSuccessfulScore += HandleSuccessfulThrow;


        List<RestartMenuController> objectsInScene = new List<RestartMenuController>();

        foreach (RestartMenuController go in Resources.FindObjectsOfTypeAll(typeof(RestartMenuController)) as RestartMenuController[])
        {
            //if (!UnityEditor.EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
            objectsInScene.Add(go);
        }

        restartMenuController = objectsInScene[0];
        restartMenuController.OnRestartButtonPressed += HandleOnRestartButtonPressed;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void HandleSuccessfulThrow()
    {
        successfulThrowCount++;

        MakeCirclesGreen();
    }

    void MakeCirclesGreen()
    {
        if (successfulThrowCount > 0)
        {
            firstCircleImage.sprite = GREEN_CIRCLE;
        }
        if (successfulThrowCount > 1)
        {
            secondCircleImage.sprite = GREEN_CIRCLE;
        }
        if (successfulThrowCount > 2)
        {
            thirdCircleImage.sprite = GREEN_CIRCLE;
        }
    }

    void MakeCirclesGray()
    {

        firstCircleImage.sprite = GRAY_CIRCLE;

        secondCircleImage.sprite = GRAY_CIRCLE;

        thirdCircleImage.sprite = GRAY_CIRCLE;

    }

    void HandleOnRestartButtonPressed()
    {
        successfulThrowCount = 0;

        MakeCirclesGray();

    }

}
