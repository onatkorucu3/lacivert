﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartMenuController : MonoBehaviour
{


    public static bool gameIsPaused;

    public GameObject startMenuUI;
    public GameObject gameUICanvas;

    public event Action OnRestartButtonPressed;

    public GameObject gameController;
    BallDragAndThrow ballDragAndThrow;


    public GameObject restartMenu;
    RestartMenuController restartMenuController;



    void OnEnable() {
        ballDragAndThrow = gameController.GetComponent<BallDragAndThrow>();

        //restartMenuController = restartMenu.GetComponent<RestartMenuController>();

        FreezeGame();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FreezeGame()
    {
        gameObject.SetActive(true);
        Time.timeScale = 0f;
        gameUICanvas.SetActive(false);
        ballDragAndThrow.enabled = false;
        gameIsPaused = true;
    }

    public void RestartGame()
    {


        //Time.timeScale = 1f;
        //gameIsPaused = false;
        gameUICanvas.SetActive(true);
        ballDragAndThrow.enabled = true;

        if (OnRestartButtonPressed != null)
        {
            OnRestartButtonPressed();
        }

        //restartMenuController.enabled = false;
        gameObject.SetActive(false);
        Debug.Log("End");
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
