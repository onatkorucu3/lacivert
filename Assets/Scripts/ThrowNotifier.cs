﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowNotifier : MonoBehaviour
{
    bool isScoringBall;

    public float distanceToCamera;
    public Camera MainCamera;

    public event Action OnFailedThrow;

    void Awake()
    {
        MainCamera = FindObjectOfType<Camera>();
    }

    // Start is called before the first frame update
    void Start()
    {
        isScoringBall = false;
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.CompareTag("Goal"))
        {
            isScoringBall = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        distanceToCamera = Vector3.Distance(gameObject.transform.position, MainCamera.transform.position);

        if (distanceToCamera > 50f)
        {
            if (!isScoringBall)
            {

                if (OnFailedThrow != null)
                {
                    OnFailedThrow();
                }
            }
            gameObject.SetActive(false);
        }
        else {
            gameObject.SetActive(true);
        }
    }
}
