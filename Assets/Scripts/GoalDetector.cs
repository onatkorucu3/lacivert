﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalDetector : MonoBehaviour
{
    public AudioSource scoreBallAudio;
    public event Action OnSuccessfulScore;

    // Start is called before the first frame update
    void Start()
    {
        scoreBallAudio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider otherCollider)
    {
        if (otherCollider.CompareTag("Ball"))
        {
            scoreBallAudio.Play();
            OnSuccessfulScore();
        }
    }
}
