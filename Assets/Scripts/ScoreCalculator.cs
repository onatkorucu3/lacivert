﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCalculator : MonoBehaviour
{
    public int totalScore;
    public int streakCount;
    public int uniqueSuccessfulThrowCount;
    public int uniqueThrowCount;

    GoalDetector goalDetector;
    ThrowNotifier[] throwNotifierList;
    //RestartMenuController restartMenuController;

    public GameObject restartMenuCanvas;
    public GameObject gameUICanvas;

    //public GameObject gameController;


    BallDragAndThrow ballDragAndThrow;

    RestartMenuController restartMenuController;

    public GameObject[] gameScoreTextList;


    void Awake()
    {
        totalScore = 0;
        streakCount = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        uniqueThrowCount = 0;

        goalDetector = FindObjectOfType<GoalDetector>();
        goalDetector.OnSuccessfulScore += HandleSuccessfulScore;

        throwNotifierList = FindObjectsOfType<ThrowNotifier>();

        foreach (ThrowNotifier throwNotifier in throwNotifierList) {
            throwNotifier.OnFailedThrow += HandleFailedThrow; //TODO: Rewrite     
        }

        //gameScoreText  = restartMenuCanvas.GetComponent.Text;


        //gameUICanvas = GameObject.FindGameObjectsWithTag("GameUI")[0];
        //restartMenuCanvas = GameObject.FindGameObjectsWithTag("RestartMenu")[0];


        //restartMenuController = FindObjectOfType<RestartMenuController>();
        //restartMenuController.OnSuccessfulScore += HandleSuccessfulScore;
        ballDragAndThrow = gameObject.GetComponent<BallDragAndThrow>();



        List<RestartMenuController> objectsInScene = new List<RestartMenuController>();

        foreach (RestartMenuController go in Resources.FindObjectsOfTypeAll(typeof(RestartMenuController)) as RestartMenuController[])
        {
            //if (!UnityEditor.EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
            objectsInScene.Add(go);
        }

        restartMenuController = objectsInScene[0];
        restartMenuController.OnRestartButtonPressed += HandleOnRestartButtonPressed;



        if (gameScoreTextList == null)
            gameScoreTextList = GameObject.FindGameObjectsWithTag("GameScoreText");
    }

    void HandleOnRestartButtonPressed()
    {

        uniqueThrowCount = 0;
        uniqueSuccessfulThrowCount = 0;
        streakCount = 0;
        totalScore = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if (uniqueThrowCount == 5) {

            uniqueThrowCount = 0;
            ballDragAndThrow.enabled = false;

            gameUICanvas.SetActive(false);
            restartMenuCanvas.SetActive(true);
            //restartMenuCanvas.GetComponent<GameScoreText>().Text;
        }

        gameScoreTextList[0].GetComponent<Text>().text = totalScore.ToString();
    }


    private void HandleSuccessfulScore()
    {
        uniqueThrowCount++;

        if (streakCount < 3)
        {
            streakCount++;
        }
        totalScore = totalScore + streakCount;

        uniqueSuccessfulThrowCount++;

        Debug.Log("Successful! totalScore"+totalScore);

        Debug.Log("uniqueThrowCount" + uniqueThrowCount);

    }

    private void HandleFailedThrow()
    {
        uniqueThrowCount++;
        streakCount = 0;
        Debug.Log("Failed! totalScore" + totalScore);
        Debug.Log("uniqueThrowCount" + uniqueThrowCount);


    }



}
