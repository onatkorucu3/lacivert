﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDragAndThrow : MonoBehaviour
{
    Vector3 pointOfFirstTouch;
    Vector3 pointOfSecondTouch;

    Vector3 throwingForce;
    public Vector3 minimumThrowingForce;
    public Vector3 maximumThrowingForce;

    Vector3 rotatingForce;

    public float forceMultiplier = 10f;

    public List<GameObject> ballListToThrow;

    public GameObject ballToThrow;

    public GameObject trajectoryStartPoint;
    Vector3 dragPosition;
    Vector3 temporaryThrowingForce;
    public GameObject trajectoryDot;
    GameObject[] trajectoryDotList;
    public int numberOfTrajectoryDots;
    Vector3 trajectoryLineStart;
    Vector3 positionChangeInTimeUnit;
    Vector3 positionChangeBecauseOfGravity;
    Vector3 resultingPosition;

    int ballIndex;
    public int basketballBallCount;
    BasketballBallListHandler basketballBallListHandler;

    Vector3 riseAmount;
    public float riseSpeed = 5.0f;

    public event Action OnBallThrow;

    public AudioSource hitBallAudio;

    public bool isThrowAllowed;

    ThrowNotifier throwNotifier;

    RestartMenuController restartMenuController;


    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        hitBallAudio = GetComponent<AudioSource>();

        trajectoryDotList = new GameObject[numberOfTrajectoryDots];
        InstantiateTrajectoryLine();

        ballIndex = 0;

        basketballBallListHandler = FindObjectOfType<BasketballBallListHandler>();
        basketballBallCount = basketballBallListHandler.basketballBallList.Count;

        ballToThrow = basketballBallListHandler.basketballBallList[ballIndex];

        //throwNotifier = FindObjectOfType<ThrowNotifier>();
        //throwNotifier.OnThrowedBallIsFarAway += MakeNewThrowAvailable;

        isThrowAllowed = true;



        List<RestartMenuController> objectsInScene = new List<RestartMenuController>();

        foreach (RestartMenuController go in Resources.FindObjectsOfTypeAll(typeof(RestartMenuController)) as RestartMenuController[])
        {
            //if (!UnityEditor.EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
                objectsInScene.Add(go);
        }

        restartMenuController = objectsInScene[0];
        restartMenuController.OnRestartButtonPressed += HandleOnRestartButtonPressed;
    }

    // Update is called once per frame
    void Update()
    {
        
            if (Input.GetMouseButtonDown(0) && isThrowAllowed)
            {
                pointOfFirstTouch = Input.mousePosition;
            }

            if (Input.GetMouseButton(0) && isThrowAllowed)
            {

                dragPosition = Input.mousePosition;
                temporaryThrowingForce = new Vector3(Mathf.Clamp(pointOfFirstTouch.x - dragPosition.x, minimumThrowingForce.x, maximumThrowingForce.x)
                   , Mathf.Clamp(pointOfFirstTouch.y - dragPosition.y, minimumThrowingForce.y, maximumThrowingForce.y)
                   , Mathf.Clamp(110, minimumThrowingForce.z, maximumThrowingForce.z)); //TODO: Change angle maybe?//TODO: Don't use magic number.

                for (int i = 0; i < numberOfTrajectoryDots; i++)
                {
                    trajectoryDotList[i].SetActive(true);
                    trajectoryDotList[i].transform.position = CalculatePosition(i * 0.01f); //TODO: Don't use magic number.
                }
            }

            if (Input.GetMouseButtonUp(0) && isThrowAllowed)
            {
                pointOfSecondTouch = Input.mousePosition;

                float dragAmountInYAxis = pointOfFirstTouch.y - pointOfSecondTouch.y;

                foreach (GameObject dot in trajectoryDotList)
                {
                    dot.SetActive(false);
                }

                if (dragAmountInYAxis > 35)
                { //TODO: Don't use magic number.
                    ThrowBall();
                    SwitchToNewBall();
                    StartCoroutine(DisableThrowTemporarily());
                }

            }

        
    }

    void HandleOnRestartButtonPressed()
    {
        //foreach(GameObject go in basketballBallListHandler.basketballBallList)
        //{
        //    go
        //}

        ballIndex = 0;
        ballToThrow = basketballBallListHandler.basketballBallList[3];
        //ballToThrow.isKinematic = true;
        isThrowAllowed = true;

    }

    void ThrowBall()
    {
        //Destroy(ballToThrow);
        hitBallAudio.Play();

        throwingForce = new Vector3(Mathf.Clamp(pointOfFirstTouch.x - pointOfSecondTouch.x, minimumThrowingForce.x, maximumThrowingForce.x)
               , Mathf.Clamp(pointOfFirstTouch.y - pointOfSecondTouch.y, minimumThrowingForce.y, maximumThrowingForce.y)
               , Mathf.Clamp(110, minimumThrowingForce.z, maximumThrowingForce.z)); //TODO: Change angle maybe?

        ballToThrow.GetComponent<Rigidbody>().AddForce(throwingForce * forceMultiplier, ForceMode.Impulse);

        rotatingForce = new Vector3(Mathf.Clamp(pointOfFirstTouch.x - pointOfSecondTouch.x, minimumThrowingForce.x, maximumThrowingForce.x)
            , Mathf.Clamp(pointOfFirstTouch.y - pointOfSecondTouch.y, minimumThrowingForce.y, maximumThrowingForce.y)
            , Mathf.Clamp(0, minimumThrowingForce.z, maximumThrowingForce.z)); //TODO: Add minimum and maximum "Rotating" force.

        ballToThrow.GetComponent<Rigidbody>().AddTorque(rotatingForce * forceMultiplier, ForceMode.Impulse);

        ballToThrow.GetComponent<Rigidbody>().useGravity = true;

        isThrowAllowed = false;
        OnBallThrow();
    }

    void SwitchToNewBall()
    {
        if (ballIndex < basketballBallCount - 1)
        {
            Debug.Log("ballIndex " + ballIndex);
            Debug.Log(" basketballBallCount" + basketballBallCount);

            ballIndex++;
            ballToThrow = basketballBallListHandler.basketballBallList[ballIndex];
            //RaiseRemainingBalls();
        }
    }

    void RaiseRemainingBalls()
    {
        riseAmount = new Vector3(0, 80f, 0);

        for (int i = ballIndex; i < basketballBallCount; i++)
        {

            Vector3 startPosition = basketballBallListHandler.basketballBallList[ballIndex].transform.position;
            Vector3 targetPosition = startPosition + riseAmount;


            float step = riseSpeed * Time.deltaTime;
            basketballBallListHandler.basketballBallList[ballIndex].transform.position = Vector3.MoveTowards(startPosition, targetPosition, step);
        }
    }

    void InstantiateTrajectoryLine()
    {

        for (int i = 0; i < numberOfTrajectoryDots; i++)
        {
            trajectoryDotList[i] = Instantiate(trajectoryDot, trajectoryStartPoint.transform); //TODO: USE OBJECT POOLING.
        }
    }

    private Vector3 CalculatePosition(float elapsedTime)
    {

        trajectoryLineStart = new Vector3(trajectoryStartPoint.transform.position.x
            , trajectoryStartPoint.transform.position.y
            , trajectoryStartPoint.transform.position.z);


        temporaryThrowingForce = new Vector3(Mathf.Clamp(pointOfFirstTouch.x - dragPosition.x, minimumThrowingForce.x, maximumThrowingForce.x)
               , Mathf.Clamp(pointOfFirstTouch.y - dragPosition.y, minimumThrowingForce.y, maximumThrowingForce.y)
               , Mathf.Clamp(110, minimumThrowingForce.z, maximumThrowingForce.z));

        positionChangeInTimeUnit = temporaryThrowingForce * elapsedTime;

        positionChangeBecauseOfGravity = 0.5f * elapsedTime * elapsedTime * Physics.gravity * ballToThrow.GetComponent<Rigidbody>().mass * ballToThrow.GetComponent<Rigidbody>().mass;

        //Debug.Log("Physics.gravity is: " + Physics.gravity);
        //Debug.Log("positionChangeBecauseOfGravity is: "+ positionChangeBecauseOfGravity);
        //Debug.Log("positionChangeBecauseOfGravity.y is: " + positionChangeBecauseOfGravity.y);
        //Debug.Log("elapsedTime is: " + elapsedTime);


        resultingPosition = new Vector3(trajectoryLineStart.x + positionChangeInTimeUnit.x //+ positionChangeBecauseOfGravity.x
            , trajectoryLineStart.y + positionChangeInTimeUnit.y + positionChangeBecauseOfGravity.y
            , trajectoryLineStart.z + positionChangeInTimeUnit.z //+ positionChangeBecauseOfGravity.z
        );

        return resultingPosition;

    }

    IEnumerator DisableThrowTemporarily()
    {
        isThrowAllowed = false;
        yield return new WaitForSeconds(0.0f);
        isThrowAllowed = true;
    }
}
