﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StreakBonusDisplayer : MonoBehaviour
{

    GoalDetector goalDetector;
    public int streakCount;
    public Text streakCountText;
    ThrowNotifier[] throwNotifierList;
    RestartMenuController restartMenuController;


    // Start is called before the first frame update
    void Start()
    {
        streakCount = 0;
        goalDetector = FindObjectOfType<GoalDetector>();
        goalDetector.OnSuccessfulScore += HandleSuccessfulScore;

        streakCountText = gameObject.GetComponent<Text>();

        throwNotifierList = FindObjectsOfType<ThrowNotifier>();

        foreach (ThrowNotifier throwNotifier in throwNotifierList)
        {
            throwNotifier.OnFailedThrow += HandleFailedThrow; //TODO: Rewrite     
        }

        List<RestartMenuController> objectsInScene = new List<RestartMenuController>();

        foreach (RestartMenuController go in Resources.FindObjectsOfTypeAll(typeof(RestartMenuController)) as RestartMenuController[])
        {
            if (!UnityEditor.EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
                objectsInScene.Add(go);
        }

        restartMenuController = objectsInScene[0];
        restartMenuController.OnRestartButtonPressed += HandleOnRestartButtonPressed;
    }

    void HandleOnRestartButtonPressed()
    {
        streakCountText.text = "";
        streakCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (streakCount > 0)
        {
            streakCountText.text = "X" + streakCount.ToString();
        }
        else {
            streakCountText.text = "";
        }
    }

    void HandleSuccessfulScore()
    {
        if (streakCount < 3)
        {
            streakCount++;
        }
    }

    void HandleFailedThrow()
    {
        streakCount = 0;
    }
}
