﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceAudioPlayer : MonoBehaviour
{
    public AudioSource bounceBallAudio;

    // Start is called before the first frame update
    void Start()
    {
        bounceBallAudio = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        bounceBallAudio.Play();

    }
}
