﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RemainingBallDisplayer : MonoBehaviour
{
    public int displayedRemainingBallCount; 

    BallDragAndThrow ballDragAndThrow;
    BasketballBallListHandler basketballBallListHandler;

    RestartMenuController restartMenuController;


    // Start is called before the first frame update
    void Start()
    {
        basketballBallListHandler = FindObjectOfType<BasketballBallListHandler>();
        displayedRemainingBallCount = basketballBallListHandler.basketballBallList.Count;

        ballDragAndThrow = FindObjectOfType<BallDragAndThrow>();
        ballDragAndThrow.OnBallThrow += DecreaseDisplayedRemainingBallCount;


        List<RestartMenuController> objectsInScene = new List<RestartMenuController>();

        foreach (RestartMenuController go in Resources.FindObjectsOfTypeAll(typeof(RestartMenuController)) as RestartMenuController[])
        {
            //if (!UnityEditor.EditorUtility.IsPersistent(go.transform.root.gameObject) && !(go.hideFlags == HideFlags.NotEditable || go.hideFlags == HideFlags.HideAndDontSave))
            objectsInScene.Add(go);
        }

        restartMenuController = objectsInScene[0];
        restartMenuController.OnRestartButtonPressed += HandleOnRestartButtonPressed;


    }

    void HandleOnRestartButtonPressed()
    {

        displayedRemainingBallCount = basketballBallListHandler.basketballBallList.Count;

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("displayedRemainingBallCount"+displayedRemainingBallCount);
        gameObject.GetComponent<Text>().text = displayedRemainingBallCount.ToString();
    }

    void DecreaseDisplayedRemainingBallCount()
    {
        if (displayedRemainingBallCount > 0) {
            displayedRemainingBallCount--;
        }
    }
}
